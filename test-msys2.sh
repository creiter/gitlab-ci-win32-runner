#!/bin/bash

set -e

if [[ "$MSYSTEM" == "MINGW32" ]]; then
    export MSYS2_ARCH="i686"
else
    export MSYS2_ARCH="x86_64"
fi

# Update everything
pacman --noconfirm -Suy --force

# Install the required packages
pacman --noconfirm -S --force --needed \
    base-devel \
    mingw-w64-$MSYS2_ARCH-toolchain

# Do your testing
which gcc
gcc --version
