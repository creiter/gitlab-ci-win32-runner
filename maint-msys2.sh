#!/bin/bash

set -e

# clean the runner cache +  builds
rm -Rf /c/r/cache
rm -Rf /c/r/builds || true  # will fail to remove this build

# Update the msys2 install
pacman --noconfirm -Suy  --overwrite '**'
pacman -S  --overwrite '**' --needed --noconfirm base-devel mingw-w64-i686-toolchain mingw-w64-x86_64-toolchain
pacman -Scc --noconfirm

# make the msys2 tree ready for a git update
cd /
echo '* binary' > .gitattributes
find . -type d -empty -exec touch {}/.gitignore \; || true;

# now we need to leave the bash and continue in cmd
# ...
