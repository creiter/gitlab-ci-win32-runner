========================
GitLab CI Windows Runner
========================

.. image:: https://gitlab.gnome.org/creiter/gitlab-ci-win32-runner/badges/master/pipeline.svg
    :target: https://gitlab.gnome.org/creiter/gitlab-ci-win32-runner/commits/master

Please file a bug here if something is not working.

Short overview:

* It's named "win32-creiter" and only runs jobs tagged as "win32"
* cmd.exe is used to execute commands in ".gitlab-ci.yml"
* When starting CWD is in the git repo
* PATH is the default except for a portable git install
* MSYS2 is in installed in "C:/msys64"
* The machine runs Windows Server 2012 R2 (~Windows 8)

How to use it:

* Best to test things on a real Windows machine/VM first with MSYS2 installed
* For a working example see https://gitlab.gnome.org/GNOME/pygobject

MSYS2 specific issues:

* Like Arch MSYS2 does not allow installing packages without updating the
  system first. Installing a package without updating risks not updating
  a package which had a soname bump in the meantime and is used by the
  newly installed package
* Unlike Arch MSYS2 has a system upgrade phase where when one of the core
  packages has an update only the core is updated and then the shell has
  to be restarted. Only after that can normal upgrades happen. This is due to
  Windows not allowing to replace files which are in use
* You can just copy what pygobject is doing

See setup.rst for how to the server was set up.
