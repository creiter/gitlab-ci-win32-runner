====================
Windows Runner Setup
====================

Hetzner Cloud Specific:

* Using a Hetzner Cloud CX21 instance, ~6€ a month
* Using Windows Server 2012 R2 (EOL probably 2023)
* Follow https://wiki.hetzner.de/index.php/Windows_on_Cloud/en

Generic:

* In server manager disable IE security for admins and users so
  that iexplorer works
* Install Windows updates
* Activate Windows

In case of little disk space (this saves a few gigs):

* In server manager install desktop experience feature
* Reboot
* Powershell: ``Get-WindowsFeature | Where-Object -FilterScript { $_.Installed -Eq $FALSE } | Uninstall-WindowsFeature -Remove``
* Right click main drive, properties, do a full disk cleanup
* cmd: ``dism /online /cleanup-image /startcomponentcleanup /resetbase``

Finally:

* Disable Windows Error Reporting (otherwise crashing programs get stuck):
  https://www.raymond.cc/blog/disable-program-has-stopped-working-error-dialog-in-windows-server-2008/
* Add non-admin Windows user, log in as user
* Download gitlab runner executable, copy to ``C:/r/``
  (see https://docs.gitlab.com/runner/install/windows.html)
* Download portable git, install to ``C:/r/git``
* Add ``C:/r/git/cmd`` to ``PATH`` (advanced system preferences)
* Install msys2 to ``C:/msys64`` and follow instructions on msys2.org
* Execute the script below in the msys2 console to install the base packages
  and commit everything to git so it can be reset between CI jobs
* Open cmd in ``C:/r/``
* Register runner: ``gitlab-runner register``
* Adjust config.toml for build/cache dir, cleanup (see toml file below)
* Start runner ``gitlab-runner run`` (no service install, as we need a GUI)
* Enable autologon for the user using sysinternals autologon
* Add a shortcut to the gitlab runner in the autorun directory and adjust
  the executed command by appending " run"

freeze-msys2.sh:

.. code:: shell

    #!/bin/bash

    set -e

    cd /
    pacman -Suy
    pacman -S --needed --noconfirm base-devel mingw-w64-i686-toolchain mingw-w64-x86_64-toolchain
    pacman -Scc --noconfirm
    echo '* binary' > .gitattributes
    find . -type d -empty -exec touch {}/.gitignore \; || true;
    export PATH=/c/r/git/cmd:"$PATH"

    git init .
    git config user.email "foo@bar"
    git config user.name "Foo Bar"
    git add -v .
    git commit -m "init"
    git gc
    echo "done"

config.toml:

.. code:: yaml

    [[runners]]
      name = "win32-creiter"
      url = "https://gitlab.gnome.org"
      token = "YOUR_SECRET_TOKEN"
      executor = "shell"
      shell = "cmd"
      builds_dir="C:/r/builds/"
      builds_cache="C:/r/cache/"
      pre_clone_script = "cmd /c pre_clone_script.bat"

pre_clone_script.bat:

.. code::

    taskkill /f /fi "USERNAME eq %username%" /fi "IMAGENAME ne explorer.exe" /fi "IMAGENAME ne cmd.exe" /fi "IMAGENAME ne gitlab-runner.exe" /fi "IMAGENAME ne conhost.exe"
    C:/msys64/usr/bin/bash -lc "ps | awk '{print $1}' | xargs kill -9"
    del C:\msys64\.git\index.lock
    git -C C:/msys64 clean -qxfd --exclude=*.pkg.tar.xz
    git -C C:/msys64 reset -q HEAD --hard
